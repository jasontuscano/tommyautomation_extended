Feature: Cookie Settings Feature
  I want to test the cookie settings

  Scenario Outline: Enabling and Disabling Cookies
    Given I have navigated to the application
    When I check and uncheck "<analytical>" and "<socialMedia>" cookie
    Then I verify the <analyticalCookieStatus> and <socialMediaCookieStatus>

    Examples: 
      | analytical | socialMedia  | analyticalCookieStatus | socialMediaCookieStatus |
      |     false  | false        | Reject                 | Reject                  |
      |     true   | false        | Accept                 | Reject                  |
      |     false  | true         | Reject                 | Accept                  |
      |     true   | true         | Accept                 | Accept                  |
   
   @Newsletter
   Scenario: Disable Newsletter Popup
   	Given I have navigated to the application
   	When I disable newsletter popup
   	And I click on Kids menu
   	Then The newsletter popup should not be displayed 
