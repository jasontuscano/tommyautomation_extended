package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="classpath:features", plugin= {"json:report.json", "html:cucumber", "pretty"},
monochrome=true, glue="stepDefs")
public class TestRunner {
	
}
