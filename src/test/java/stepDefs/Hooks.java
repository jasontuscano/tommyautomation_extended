package stepDefs;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.AppiumDriver;
import library.DriverLibrary;

public class Hooks {

	DriverLibrary lib = new DriverLibrary();
	public static WebDriver driver = null;
	public static AppiumDriver<?> mobileDriver = null;
	String env = DriverLibrary.getProperty("Environment");
}
