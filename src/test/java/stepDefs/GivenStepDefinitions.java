package stepDefs;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class GivenStepDefinitions extends Hooks {
	
	@Before
	public void setUp() {
		if(env.equalsIgnoreCase("Desktop")) {
			if(driver == null) {
				driver = lib.getDriver();
			}
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			if(mobileDriver == null) {
				mobileDriver = lib.getMobileDriver();
			}
		}
	}
	
	@After
	public void tearDown() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(env.equalsIgnoreCase("Desktop")) {
			if(driver != null) {
				driver.quit();
				driver = null;
			}
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			if(mobileDriver != null) {
				mobileDriver.quit();
				mobileDriver = null;
			}
		}
	}
	
	@Given("^I have navigated to the application$")
	public void i_have_navigated_to_the_application() throws Throwable {
	    lib.navigateToURL();
	}
}
