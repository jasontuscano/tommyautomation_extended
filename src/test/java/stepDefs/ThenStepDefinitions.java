package stepDefs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import cucumber.api.java.en.Then;;

public class ThenStepDefinitions extends Hooks {

	List<String> cookieStatus = lib.getCookieStatus();
	String analyticalCookieStatus = cookieStatus.get(0);
	String socialMediaCookieStatus = cookieStatus.get(1);
	String functionalCookieStatus = cookieStatus.get(2);
	
	@Then("^I verify the Reject and Reject$")
	public void i_verify_the_Reject_and_Reject() throws Throwable {
	    
	    assertEquals("Reject", analyticalCookieStatus);
	    assertEquals("Reject", socialMediaCookieStatus);
	    assertEquals("Accept", functionalCookieStatus);
	}

	@Then("^I verify the Accept and Reject$")
	public void i_verify_the_Accept_and_Reject() throws Throwable {
		assertEquals("Accept", analyticalCookieStatus);
	    assertEquals("Reject", socialMediaCookieStatus);
	    assertEquals("Accept", functionalCookieStatus);
	}

	@Then("^I verify the Reject and Accept$")
	public void i_verify_the_Reject_and_Accept() throws Throwable {
		assertEquals("Reject", analyticalCookieStatus);
		assertEquals("Accept", socialMediaCookieStatus);
		assertEquals("Accept", functionalCookieStatus);
	}

	@Then("^I verify the Accept and Accept$")
	public void i_verify_the_Accept_and_Accept() throws Throwable {
		assertEquals("Accept", analyticalCookieStatus);
		assertEquals("Accept", socialMediaCookieStatus);
		assertEquals("Accept", functionalCookieStatus);
	}
	
	@Then("^The newsletter popup should not be displayed$")
	public void the_newsletter_popup_should_not_be_displayed() throws Throwable {
	   assertFalse(lib.verifyNewsLetterPopup());
	}
}
