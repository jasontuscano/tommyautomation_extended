package stepDefs;

import cucumber.api.java.en.When;

public class WhenStepDefinitions extends Hooks{

	@When("^I check and uncheck \"([^\"]*)\" and \"([^\"]*)\" cookie$")
	public void i_check_and_uncheck_and_cookie(String analytical, String socialMedia) throws Throwable {
		boolean analyticalCookie = Boolean.valueOf(analytical);
		boolean socialMediaCookie = Boolean.valueOf(socialMedia);
		lib.clickMoreInfoLink();
		lib.changeCookieSettings(analyticalCookie, socialMediaCookie);
		lib.saveCookiePreference();

	}

	@When("^I disable newsletter popup$")
	public void i_disable_newsletter_popup() throws Throwable {
		lib.closeLocationPopup();
		lib.closeCookiePopup();
		lib.disableNewsLetterPopup();
	}


	@When("^I click on Kids menu$")
	public void i_click_on_Kids_menu() throws Throwable {
		lib.navigateToKidsSection();
	}
}
