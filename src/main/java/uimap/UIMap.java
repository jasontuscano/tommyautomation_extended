package uimap;

import org.openqa.selenium.By;

public class UIMap {

	public static By moreInfoLink = By.xpath("//a[contains(text(),'More Info')]");
	public static By cookiePopup = By.xpath("//div[@class='cookie-notice__main']");
	public static By cookiePreferenceTitle = By.xpath("//div[@class='cookie-notice__preferences']/h4[@class='cookie-notice__title']");
	public static By cookieAnalyticalCheckBox = By.id("cookie-notice-analytical");
	public static By cookieSocialMediaCheckBox = By.id("cookie-notice-socialmedia");
	public static By doneButton = By.xpath("//button[text()='Done']");
	public static By newsLetterPopup = By.id("remodalNewsletter");
	public static By newsLetterPopupClose = By.xpath("//div[@id='remodalNewsletter']//a[@class='remodal-close']");
	public static By locationPopupClose = By.xpath("//div[@class='btnClose']");
	public static By kidsMenuLink = By.xpath("//a[@class='navigationList mainNavItem' and contains(text(),'Kids')]");
	public static By cookiePopupClose = By.xpath("//div[contains(@class,'cookie')]//a[@class='remodal-close']");
}
