package library;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import uimap.UIMap;

public class DriverLibrary {

	public static WebDriver driver;
	public static AndroidDriver<?> mobileDriver;
	private String env = DriverLibrary.getProperty("Environment");
	private String url = DriverLibrary.getProperty("URL");
	private String driverPath = DriverLibrary.getProperty("DriverPath");
	private int timeout = Integer.parseInt(DriverLibrary.getProperty("Timeout"));
	private String appiumServerURL;
	private String deviceName;
	private String osVersion;

	public static String timestamp = "23012019";

	public static String getProperty(String name) {
		String value = new String();
		try {
			File file = new File("src/main/resources/Settings.properties");
			InputStream in = new FileInputStream(file);
			Properties props = new Properties();
			props.load(in);
			value = props.getProperty(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public WebDriver getDriver() {
		try {
			System.setProperty("webdriver.chrome.driver", driverPath);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	public AppiumDriver<?> getMobileDriver(){
		appiumServerURL = DriverLibrary.getProperty("AppiumServer");
		deviceName = DriverLibrary.getProperty("DeviceName");
		DriverLibrary.getProperty("MobileOS");
		osVersion = DriverLibrary.getProperty("OSVersion");
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, osVersion);
			caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			caps.setCapability(MobileCapabilityType.NO_RESET, true);
			mobileDriver = new AndroidDriver<MobileElement>(new URL(appiumServerURL), caps);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mobileDriver;
	}

	public boolean isElementPresent(By locator) {
		try {
			if(env.equalsIgnoreCase("Mobile")) {
				return mobileDriver.findElement(locator).isDisplayed();
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				return driver.findElement(locator).isDisplayed();
			}
			
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	public void waitForVisibility(WebElement element, String elementName) {
		WebDriverWait wait = null;
		if(env.equalsIgnoreCase("Desktop")) {
			wait = new WebDriverWait(driver, timeout);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			wait = new WebDriverWait(mobileDriver, timeout);
		}
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.err.println("Waiting for element "+ elementName + " failed");
			e.printStackTrace();
		}
	}

	public void waitForVisibility(By locator, String elementName) {
		WebDriverWait wait = null;
		
		try {
			if(env.equalsIgnoreCase("Desktop")) {
				wait = new WebDriverWait(driver, timeout);
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
			}
			else if(env.equalsIgnoreCase("Mobile")) {
				wait = new WebDriverWait(mobileDriver, timeout);
				wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			}
			
		} catch (Exception e) {
			System.err.println("Waiting for element "+ elementName + " failed");
			e.printStackTrace();
		}
	}
	
	public void waitForVisibilityWithoutException(By locator, String elementName) {
		WebDriverWait wait = null;
		if(env.equalsIgnoreCase("Desktop")) {
			wait = new WebDriverWait(driver, timeout);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			wait = new WebDriverWait(mobileDriver, timeout);
		}
		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
		} catch (Exception e) {
			
		}
	}
	
	public void waitForVisibilityWithoutException(By locator, int timeout, String elementName) {
		WebDriverWait wait = null;
		if(env.equalsIgnoreCase("Desktop")) {
			wait = new WebDriverWait(driver, timeout);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			wait = new WebDriverWait(mobileDriver, timeout);
		}
		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
		} catch (Exception e) {
			
		}
	}
	
	public void waitForInvisibility(By locator, String elementName) {
		WebDriverWait wait = null;
		if(env.equalsIgnoreCase("Desktop")) {
			wait = new WebDriverWait(driver, timeout);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			wait = new WebDriverWait(mobileDriver, timeout);
		}
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			System.err.println("Waiting for element "+ elementName + " failed");
			e.printStackTrace();
		}
	}

	public void waitForInvisibilityWithoutException(By locator, String elementName) {
		WebDriverWait wait = null;
		if(env.equalsIgnoreCase("Desktop")) {
			wait = new WebDriverWait(driver, timeout);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			wait = new WebDriverWait(mobileDriver, timeout);
		}
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			
		}
	}
	
	public void click(WebElement element) {
		try {
			element.click();
		} catch (Exception e) {
			jsClick(element);
		}
	}

	public void jsClick(WebElement element) {
		if(env.equalsIgnoreCase("Desktop")) {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", element);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			JavascriptExecutor js = (JavascriptExecutor)mobileDriver;
			js.executeScript("arguments[0].click();", element);
		}
	}
	public void navigateToURL() {
		if(env.equalsIgnoreCase("Desktop")) {
			driver.get(url);
		}
		else if(env.equalsIgnoreCase("Mobile")) {
			mobileDriver.get(url);
		}
		System.out.println("Navigated to " + url);
	}
	
	public void clickMoreInfoLink() {
		waitForVisibilityWithoutException(UIMap.cookieAnalyticalCheckBox, 5, "Analytical Cookie checkbox");
		closeLocationPopup();
		try {
			waitForVisibility(UIMap.cookiePopup, "Cookie Popup");
			if(env.equalsIgnoreCase("Mobile")) {
				click(mobileDriver.findElement(UIMap.moreInfoLink));
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				click(driver.findElement(UIMap.moreInfoLink));
			}
			System.out.println("Clicked More Info link");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void changeCookieSettings(boolean analyticalCookie, boolean socialMediaCookie) {
		WebElement analyticalCookieCheckBox = null;
		WebElement socialMediaCookieCheckBox = null;
		if(env.equalsIgnoreCase("Mobile")) {
			analyticalCookieCheckBox = mobileDriver.findElement(UIMap.cookieAnalyticalCheckBox);
			socialMediaCookieCheckBox = mobileDriver.findElement(UIMap.cookieSocialMediaCheckBox);
		}
		else if(env.equalsIgnoreCase("Desktop")) {
			analyticalCookieCheckBox = driver.findElement(UIMap.cookieAnalyticalCheckBox);
			socialMediaCookieCheckBox = driver.findElement(UIMap.cookieSocialMediaCheckBox);
		}
		if(analyticalCookie) {
			if(!analyticalCookieCheckBox.isSelected()) {
				click(analyticalCookieCheckBox);
				System.out.println("Enabled Analytical cookie");
			}
		}
		else {
			if(analyticalCookieCheckBox.isSelected()) {
				click(analyticalCookieCheckBox);
				System.out.println("Disabled Analytical cookie");
			}
		}
		
		if(socialMediaCookie) {
			if(!socialMediaCookieCheckBox.isSelected()) {
				click(socialMediaCookieCheckBox);
				System.out.println("Enabled Social Media cookie");
			}
		}
		else {
			if(socialMediaCookieCheckBox.isSelected()) {
				click(socialMediaCookieCheckBox);
				System.out.println("Disabled Social Media cookie");
			}
		}
	}
	
	public void saveCookiePreference() {
		try {
			waitForVisibility(UIMap.doneButton, "Done button");
			if(env.equalsIgnoreCase("Mobile")) {
				click(mobileDriver.findElement(UIMap.doneButton));
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				click(driver.findElement(UIMap.doneButton));
			}
			
			System.out.println("Clicked Done button");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<String> getCookieStatus() {
		List<String> cookieStatus = new ArrayList<String>();
		String functionalCookieStatus = new String();
		String analyticalCookieStatus = new String();
		String socialMediaCookieStatus = new String();
		try {
			Set<Cookie> cookies = null;
			if(env.equalsIgnoreCase("Mobile")) {
				cookies = mobileDriver.manage().getCookies();
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				cookies = driver.manage().getCookies();
			}
			for(Cookie c : cookies) {
				if(c.getName().equalsIgnoreCase("PVH_COOKIES_GDPR")) {
					functionalCookieStatus = c.getValue();
					
				}
				if(c.getName().equalsIgnoreCase("PVH_COOKIES_GDPR_ANALYTICS")) {
					analyticalCookieStatus = c.getValue();
				}
				if(c.getName().equalsIgnoreCase("PVH_COOKIES_GDPR_SOCIALMEDIA")) {
					socialMediaCookieStatus = c.getValue();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("Functional Cookie : "+functionalCookieStatus);
		System.out.println("Analytical Cookie : "+analyticalCookieStatus);
		System.out.println("Social Media Cookie : "+socialMediaCookieStatus);
		cookieStatus.add(analyticalCookieStatus);
		cookieStatus.add(socialMediaCookieStatus);
		cookieStatus.add(functionalCookieStatus);
		
		return cookieStatus;
	}
	
	public void disableNewsLetterPopup() {
		Set<Cookie> cookies = null;
		if(env.equalsIgnoreCase("Mobile")) {
			cookies = mobileDriver.manage().getCookies();
		}
		else if(env.equalsIgnoreCase("Desktop")) {
			cookies = driver.manage().getCookies();
		}
		for(Cookie c : cookies) {
			if(c.getName().equalsIgnoreCase("newsletterSeen")) {
				Cookie newCookie = new Cookie("newsletterSeen", "true");
				if(env.equalsIgnoreCase("Mobile")) {
					mobileDriver.manage().deleteCookie(c);
					mobileDriver.manage().addCookie(newCookie);
				}
				else if(env.equalsIgnoreCase("Desktop")) {
					driver.manage().deleteCookie(c);
					driver.manage().addCookie(newCookie);
				}
				break;
			}
		}
	}
	
	public void navigateToKidsSection() {
		try {
			waitForVisibility(UIMap.kidsMenuLink, "Kids link");
			if(env.equalsIgnoreCase("Mobile")) {
				click(mobileDriver.findElement(UIMap.kidsMenuLink));
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				click(driver.findElement(UIMap.kidsMenuLink));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeCookiePopup() {
		waitForVisibilityWithoutException(UIMap.cookiePopupClose, 2, "Cookie popup close");
		if(isElementPresent(UIMap.cookiePopupClose)) {
			if(env.equalsIgnoreCase("Mobile")) {
				click(mobileDriver.findElement(UIMap.cookiePopupClose));
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				click(driver.findElement(UIMap.cookiePopupClose));
			}
		}
	}
	
	public boolean verifyNewsLetterPopup() {
		waitForVisibilityWithoutException(UIMap.newsLetterPopup, 5, "Newsletter popup");
		return isElementPresent(UIMap.newsLetterPopup);
	}
	
	public void closeLocationPopup() {
		if(isElementPresent(UIMap.locationPopupClose)) {
			if(env.equalsIgnoreCase("Mobile")) {
				click(mobileDriver.findElement(UIMap.locationPopupClose));
			}
			else if(env.equalsIgnoreCase("Desktop")) {
				click(driver.findElement(UIMap.locationPopupClose));
			}
		}
	}
}
