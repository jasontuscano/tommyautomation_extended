$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/CookieSettings.feature");
formatter.feature({
  "line": 1,
  "name": "Cookie Settings Feature",
  "description": "I want to test the cookie settings",
  "id": "cookie-settings-feature",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Enabling and Disabling Cookies",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I check and uncheck \"\u003canalytical\u003e\" and \"\u003csocialMedia\u003e\" cookie",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I verify the \u003canalyticalCookieStatus\u003e and \u003csocialMediaCookieStatus\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies;",
  "rows": [
    {
      "cells": [
        "analytical",
        "socialMedia",
        "analyticalCookieStatus",
        "socialMediaCookieStatus"
      ],
      "line": 10,
      "id": "cookie-settings-feature;enabling-and-disabling-cookies;;1"
    },
    {
      "cells": [
        "false",
        "false",
        "Reject",
        "Reject"
      ],
      "line": 11,
      "id": "cookie-settings-feature;enabling-and-disabling-cookies;;2"
    },
    {
      "cells": [
        "true",
        "false",
        "Accept",
        "Reject"
      ],
      "line": 12,
      "id": "cookie-settings-feature;enabling-and-disabling-cookies;;3"
    },
    {
      "cells": [
        "false",
        "true",
        "Reject",
        "Accept"
      ],
      "line": 13,
      "id": "cookie-settings-feature;enabling-and-disabling-cookies;;4"
    },
    {
      "cells": [
        "true",
        "true",
        "Accept",
        "Accept"
      ],
      "line": 14,
      "id": "cookie-settings-feature;enabling-and-disabling-cookies;;5"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 4452471524,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Enabling and Disabling Cookies",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I check and uncheck \"false\" and \"false\" cookie",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I verify the Reject and Reject",
  "matchedColumns": [
    2,
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "GivenStepDefinitions.i_have_navigated_to_the_application()"
});
formatter.result({
  "duration": 1947461072,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 21
    },
    {
      "val": "false",
      "offset": 33
    }
  ],
  "location": "WhenStepDefinitions.i_check_and_uncheck_and_cookie(String,String)"
});
formatter.result({
  "duration": 6083501920,
  "status": "passed"
});
formatter.match({
  "location": "ThenStepDefinitions.i_verify_the_Reject_and_Reject()"
});
formatter.result({
  "duration": 25078944,
  "status": "passed"
});
formatter.after({
  "duration": 2939619644,
  "status": "passed"
});
formatter.before({
  "duration": 4139384572,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Enabling and Disabling Cookies",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I check and uncheck \"true\" and \"false\" cookie",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I verify the Accept and Reject",
  "matchedColumns": [
    2,
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "GivenStepDefinitions.i_have_navigated_to_the_application()"
});
formatter.result({
  "duration": 5153468440,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 21
    },
    {
      "val": "false",
      "offset": 32
    }
  ],
  "location": "WhenStepDefinitions.i_check_and_uncheck_and_cookie(String,String)"
});
formatter.result({
  "duration": 5962129932,
  "status": "passed"
});
formatter.match({
  "location": "ThenStepDefinitions.i_verify_the_Accept_and_Reject()"
});
formatter.result({
  "duration": 152057876,
  "status": "passed"
});
formatter.after({
  "duration": 2791714628,
  "status": "passed"
});
formatter.before({
  "duration": 3842062592,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Enabling and Disabling Cookies",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I check and uncheck \"false\" and \"true\" cookie",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I verify the Reject and Accept",
  "matchedColumns": [
    2,
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "GivenStepDefinitions.i_have_navigated_to_the_application()"
});
formatter.result({
  "duration": 5727782824,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 21
    },
    {
      "val": "true",
      "offset": 33
    }
  ],
  "location": "WhenStepDefinitions.i_check_and_uncheck_and_cookie(String,String)"
});
formatter.result({
  "duration": 5752038664,
  "status": "passed"
});
formatter.match({
  "location": "ThenStepDefinitions.i_verify_the_Reject_and_Accept()"
});
formatter.result({
  "duration": 856927272,
  "status": "passed"
});
formatter.after({
  "duration": 3059893064,
  "status": "passed"
});
formatter.before({
  "duration": 6032615760,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Enabling and Disabling Cookies",
  "description": "",
  "id": "cookie-settings-feature;enabling-and-disabling-cookies;;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I check and uncheck \"true\" and \"true\" cookie",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I verify the Accept and Accept",
  "matchedColumns": [
    2,
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "GivenStepDefinitions.i_have_navigated_to_the_application()"
});
formatter.result({
  "duration": 2347733168,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 21
    },
    {
      "val": "true",
      "offset": 32
    }
  ],
  "location": "WhenStepDefinitions.i_check_and_uncheck_and_cookie(String,String)"
});
formatter.result({
  "duration": 6290822556,
  "status": "passed"
});
formatter.match({
  "location": "ThenStepDefinitions.i_verify_the_Accept_and_Accept()"
});
formatter.result({
  "duration": 82563464,
  "status": "passed"
});
formatter.after({
  "duration": 3381954904,
  "status": "passed"
});
formatter.before({
  "duration": 11290681752,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Disable Newsletter Popup",
  "description": "",
  "id": "cookie-settings-feature;disable-newsletter-popup",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 16,
      "name": "@Newsletter"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "I have navigated to the application",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "I disable newsletter popup",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I click on Kids menu",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "The newsletter popup should not be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "GivenStepDefinitions.i_have_navigated_to_the_application()"
});
formatter.result({
  "duration": 2886521168,
  "status": "passed"
});
formatter.match({
  "location": "WhenStepDefinitions.i_disable_newsletter_popup()"
});
formatter.result({
  "duration": 893612160,
  "status": "passed"
});
formatter.match({
  "location": "WhenStepDefinitions.i_click_on_Kids_menu()"
});
formatter.result({
  "duration": 2801182556,
  "status": "passed"
});
formatter.match({
  "location": "ThenStepDefinitions.the_newsletter_popup_should_not_be_displayed()"
});
formatter.result({
  "duration": 5325882584,
  "status": "passed"
});
formatter.after({
  "duration": 2739606340,
  "status": "passed"
});
});